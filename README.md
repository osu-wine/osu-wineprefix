# osu-wineprefix

A tested Wineprefix that includes everything, from dotnet, gdiplus to Japanese fonts and the like.

### Notice: Please do not remove `osuwine.sig` under Wineprefix. If you do, the script will automatically redownload the Wineprefix, overriding old ones.

## Instructions

~~1. `7z x WINE.win32.7z`~~
~~2. `mv -f WINE.win32/ your_wineprefix/`~~

1. Use the `osu-wine` script: https://gitlab.com/osu-wine/osu-wine

## Included

- `dotnet45`
- `gdiplus`
- `vlgothic`
- `corefonts`
- Several fonts copied from the Windows 10 iso

## Important stuff to not get me in trouble

#### This program is *in no way* related to Microsoft nor endorsed by Microsoft. `dotnet`, `gdiplus` and the like are products of Microsoft.

### Winetricks Log (might not be accurate)

```
w_workaround_wine_bug-34803
remove_mono
w_workaround_wine_bug-34803
remove_mono
winxp
dotnet40
dotnet45
gdiplus
corefonts
sound=alsa
sound=pulse
vlgothic
fontsmooth=rgb
sound=alsa
wenquanyi
fakechinese
takao
fakejapanese
fakejapanese_vlgothic
baekmuk
fakekorean
unifont
cjkfonts
sound=pulse
```